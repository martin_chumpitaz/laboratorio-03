package com.example.appmascotas

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnRegistrar.setOnClickListener {
            val nombre = edtNombre.text.toString()
            val edad = edtEdad.text.toString()
            var rb = ""
            var vacuna =""


            // Validaciones
            if(nombre.isEmpty()){
                toast("Debe Registrar el nombre de la mascota")
                return@setOnClickListener
            }
            if(edad.isEmpty()){
                toast("Debe registarla edad de la mascota")
                return@setOnClickListener
            }
            if(!rbPerro.isChecked && !rbGato.isChecked && !rbConejo.isChecked){
                toast("Debe seleccionar un tipo de mascota")
                return@setOnClickListener
            }else{
                if(rbPerro.isChecked){
                  rb="Perro"
                }else if(rbGato.isChecked){
                    rb="Gato"
                }else{
                    rb="Conejo"
                }
            }
            if(!chkDistemper.isChecked && !chkLeptospirosis.isChecked && !chkParvovirus.isChecked && !chkRabia.isChecked){
                toast("Debe seleccionar al menos una vacuna")
                return@setOnClickListener
            }else{
                if(chkDistemper.isChecked){
                    vacuna += "Distemper \n"
                }
                if(chkLeptospirosis.isChecked){
                    vacuna += "Leptospirosis \n"
                }
                if(chkParvovirus.isChecked){
                    vacuna += "Parvovirus \n"
                }
                if(chkRabia.isChecked){
                    vacuna += "Rabia \n"
                }
            }

            // Definir Bundle a pasar a Destino
            val bundle = Bundle()
            bundle.apply {
                putString("key_nombres", nombre)
                putString("key_edad", edad)
                putString("key_tipo",rb)
                putString("key_vacuna",vacuna)
            }


            val intent = Intent(this,DestinoActivity::class.java)
            intent.putExtras(bundle)
            startActivity(intent)

        }
    }

    fun toast(mensaje:String):Unit{
        Toast.makeText(this,"$mensaje",Toast.LENGTH_SHORT).show()
    }
}

